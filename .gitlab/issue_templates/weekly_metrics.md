<!-- 
Set Title to:

Code Suggestions Weekly Metrics - 2024-MM-DD
-->
## Code Suggestions Weekly Metrics

This issue is meant to capture the current user metrics for Code Suggestions. The data come from [Tableau](https://10az.online.tableau.com/#/site/gitlab/views/PDCodeSuggestions/ExecutiveSummary). All metrics exclude internal (GitLab) users.

## Summary/Highlights

- {+ HIGHLIGHT 1 +}
- {+ HIGHLIGHT 2 +}

## Metrics

| Metric                                          | Last Week | This Week | Change |
|-------------------------------------------------|-----------|-----------|--------|
| Number of Users                                 |           |           |        |
| Acceptance Rate - Code Completion               |           |           |        |
| Acceptance Rate - Code Generation               |           |           |        |
| Latency - Code Completion                       |           |           |        |
| Latency - Code Generation                       |           |           |        |
| Usage Percent - Code Completion                 |           |           |        |
| Usage Percent - Code Generation (Non-Streaming) |           |           |        |
| Usage Percent - Code Generation (Streaming)     |           |           |        |

## Charts

### Number of Users

{- INSERT SCREENSHOT -}

### Acceptance Rate

{- INSERT SCREENSHOT -}

### Latency

{- INSERT SCREENSHOT -}

## Sources

All information comes from Tableau:

- [Number of Users][tableau-gateway]
- [Acceptance Rate][tableau-quality]. Acceptance rate is the number of Accepted Suggestions / number of Shown Suggestions, where they have been shown for at least 750ms.
- [Latency][tableau-timing]. I am currently using the P50/Median Time to Show
- [Completion vs. Generation][tableau-ide]

<details><summary>Tableau Details</summary>

Number of Users

- [Dashboard][tableau-gateway]
- Metric: User Count
- Gateway Dimension: Delivery Type
- Date Aggregation: Week
- Delivery Type: All
- Internal vs. External: Everything but Internal

Acceptance Rate - Code Completion

- [Dashboard][tableau-quality]
- Metric: Acceptance Rate
- IDE Dimension: Model Name
- Date Aggregation: Week
- Model Name: code-gecko@002, vertex/codestral@2405
- Internal vs. External: Everything but Internal

Acceptance Rate - Code Generation Streaming

- [Dashboard][tableau-quality]
- Metric: Acceptance Rate
- IDE Dimension: Model Engine
- Date Aggregation: Week
- Response Type: Streamed
- Model Name: Null (See https://gitlab.com/gitlab-org/editor-extensions/gitlab-lsp/-/issues/188)
- Internal vs. External: Everything but Internal

Latency - Code Completion

- [Dashboard][tableau-timing]
- Metric: P50/Median
- IDE Dimension: Total
- Date Aggregation: Week
- Base Metric: Time to Show
- Response Type: All
- Model Name: code-gecko@002
- Internal vs. External: Everything but Internal

Latency - Code Generation (Streaming)

- [Dashboard][tableau-timing]
- Metric: P50/Median
- IDE Dimension: Total
- Date Aggregation: Week
- Base Metric: Stream Start Time
- Response Type: Streamed
- Model Name: Null (See https://gitlab.com/gitlab-org/editor-extensions/gitlab-lsp/-/issues/188)
- Internal vs. External: Everything but Internal

Usage Percent - Code Completion

- [Dashboard][tableau-ide]
- Metric: Shown Count
- IDE Dimension: Total
- Date Aggregation: Week
- Response Type: Non-Streamed
- Model Name: code-gecko@002
- Internal vs. External: Everything but Internal

Usage Percent - Generation (Non-Streaming)

- [Dashboard][tableau-ide]
- Metric: Shown Count
- IDE Dimension: Total
- Date Aggregation: Week
- Response Type: Non-Streamed
- Model Engine: anthropic
- Model Name: All
- Internal vs. External: Everything but Internal

Usage Percent - Generation (Streaming)

- [Dashboard][tableau-ide]
- Metric: Shown Count
- IDE Dimension: Total
- Date Aggregation: Week
- Response Type: Streamed
- Model Engine: All
- Model Name: Null (See https://gitlab.com/gitlab-org/editor-extensions/gitlab-lsp/-/issues/188)
- Internal vs. External: Everything but Internal

</details>

## Questions? Comments?

Please comment below with any questions. Or you can reach out to [#g_code_creation](https://gitlab.enterprise.slack.com/archives/C048Z2DHWGP) on Slack

<!-- Metadata below -->

[tableau-summary]: https://10az.online.tableau.com/#/site/gitlab/views/PDCodeSuggestions/ExecutiveSummary
[tableau-gateway]: https://10az.online.tableau.com/#/site/gitlab/views/PDCodeSuggestions/GatewayMetrics
[tableau-quality]: https://10az.online.tableau.com/#/site/gitlab/views/PDCodeSuggestions/QualityMetrics
[tableau-timing]:  https://10az.online.tableau.com/#/site/gitlab/views/PDCodeSuggestions/IDETimeMetrics
[tableau-ide]: https://10az.online.tableau.com/#/site/gitlab/views/PDCodeSuggestions/IDEMetrics
[slack]: https://gitlab.enterprise.slack.com/archives/C048Z2DHWGP

/label ~"group::code creation" ~"Weekly Metrics" ~"Category:Code Suggestions" 
/due in 1 week
/confidential