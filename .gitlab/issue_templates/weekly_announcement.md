<!-- Code Suggestion Status Report - YYYY-MM-DD (Friday or last working day of week) -->
## :golf: Purpose

Code suggestion is a crucial AI product feature that is rapidly evolving, with multiple teams across various functions working on it. We intend to make this feature available in GA in FY24. Our working hypothesis is that we can reach GA once the acceptance rate is adequate (20%). To ensure that executives, DRIs, and all team members contributing to the effort have a single place to understand our progress and plan, we will be sending out weekly updates. Stay tuned for more exciting developments!

## :mega: Important

### :tada: Progress - [Here's](link) last week's update

### :pause_button: Potential blockers

### :sos: Help needed/Blockers

## :chart_with_upwards_trend: [Metrics](https://app.periscopedata.com/app/gitlab/1160135/Code-Suggestions-Event-Data-(Basti's-Version))

## :ship: Shipped this week

## :crystal_ball: Planned for next week/the near future

You can edit this template [here](https://gitlab.com/gitlab-com/create-stage/code-creation/announcements/-/blob/main/.gitlab/issue_templates/weekly_announcement.md).

## :white_check_mark: ToDo

- [ ] Link to last week's update and close it - @kbychu
- [ ] Link shipped MRs based on labels (~"group::code creation" or ~"Category:Code Suggestions") in the following projects: [GitLab](https://gitlab.com/gitlab-org/gitlab), [Editor-extensions](https://gitlab.com/gitlab-org/editor-extensions), [Model Gateway](https://gitlab.com/gitlab-org/modelops/applied-ml/code-suggestions/ai-assist), [customers-gitlab-com](https://gitlab.com/gitlab-org/customers-gitlab-com), [Repository X-Ray](https://gitlab.com/gitlab-org/code-creation/repository-x-ray) - @mnohr
- [ ] Tag `@courtmeddaugh` (fulfillment), `@gdoud` (growth) `@tlinz` (AI framework) `@phikai` (Editor) `@rogerwoo` (Cloud Connector) for updates (decisions, shipped MRs, announcements) from fulfillment - @kbychu
- [ ] Tag `@vincywilson` for quality updates - @kbychu
- [ ] Review Model Eval for any meaningful updates in https://gitlab.com/gitlab-org/modelops/applied-ml/code-suggestions/prompt-library/-/issues/23 - @kbychu
- [ ] Review agenda from various meetings and summarize most pertinent points - @kbychu
- [ ] Grab latest metrics for the week - @kbychu
- [ ] Update plans for next week - @mnohr @kbychu
- [ ] Write content for :mega: Important - @kbychu
- [ ] Tag AI Leadership, and other intersted party - @kbychu
- [ ] Post on Slack #g_code_suggestions, #ai_strategy for now - @kbychu
- [ ] Create next week's update issue - @kbychu
- [ ] Delete the ToDo Section

/label ~"group::code creation" ~"Category:Code Suggestions"  ~Announcements 

/assign @kbychu @mnohr 

/confidential
